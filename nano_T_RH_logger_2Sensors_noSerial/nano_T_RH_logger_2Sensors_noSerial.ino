
// include stuff
#include <Wire.h>
#include "SHTSensor.h"
#define TCAADDR 0x70

#include <SdFat.h>
SdFat sd;
SdFile dataFile;
const char filename[] = "meas.csv";
const byte chipSelect = 8;

#include <avr/wdt.h>
// not defining macros here because it is taken care (probably by MegaCoreX)
const char signature [] = "SteMar"; // needed to check if the watchdog did the reset...
char * p = (char *) malloc (sizeof (signature)); // needed to check if the watchdog resetted.

// variables
float RH_mn;
float RH;
float T_mn;
float T;
int count;
String com;

// RTC
// Date and time functions using a DS3231 RTC connected via I2C and Wire lib
#include "RTClib.h"
RTC_DS3231 rtc;
const int timeinterval = 1; // Minutes for sleep
const int meastime = 2; //Seconds for measurement
const int pausetime = 500; // Miliseconds between measurements

// sensor
SHTSensor sht_1;
SHTSensor sht_2;

// Multiplexer ausgänge hier definieren!
const byte addr_sht_1 = 6;
const byte addr_sht_2 = 7;
const byte addr_rtc = 0;

void tcaselect(uint8_t channel) {
  if (channel > 7) {
    return;
  }
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << channel);
  Wire.endTransmission();
}

bool checkifWDTReset ()
  {
  bool rv = true;
  if (strcmp (p, signature) == 0)   // signature already there
    {
      return rv;
    }
  else
    {
    memcpy (p, signature, sizeof signature);  // copy signature into RAM
    // Serial.println(p);
    rv = false;
    return rv;
    }
  }  // end of checkifWDTReset
  
//bool checkifWDTReset2 () // this should read the reset flag register of the atmega4809, but I could not figure out how so far...
//  {
//  bool rv = true;
//  if ( rv )   // signature already there
//    { // clear bit
//      RSTFR.WDRF << 1:
//      Serial.println ("Watchdog reset.");
//      return rv;
//    }
//  else
//    {
//    Serial.println ("This is a cold start.");
//    }
//  }  // end of checkIfColdStart
void setup() {
  
  wdt_enable(WDT_PERIOD_2KCLK_gc); // should be 2 seconds, look in https://forum.arduino.cc/t/arduino-nano-every-atmega4809-watch-dog/658523/5
  // some lightshow to indicate reset.
  pinMode(13, OUTPUT);

  wdt_reset();
  if ( checkifWDTReset() )
    { // quickly sleep for some seconds before "clean" restart. Do some lightshow to indicate wdt reset occurred.
      digitalWrite(13, LOW);
      delay(70);
      digitalWrite(13,HIGH);
      delay(35);
      digitalWrite(13,LOW);  
      delay(70);
      digitalWrite(13,HIGH);
      delay(35);
      digitalWrite(13,LOW);  
      delay(70);
      digitalWrite(13,HIGH);
      delay(35);
      digitalWrite(13,LOW);
      free(p);
      wdt_reset();
      // RTC initialization - needed for resetting the alarm...
      tcaselect(addr_rtc);
      delay(100);
      while (! rtc.begin()) {
        delay(500);
      }
      wdt_reset();
      rtc.clearAlarm(2);
      rtc.disableAlarm(2);
      DateTime alarmtime = rtc.now()+TimeSpan(0,0,0,15);
      rtc.setAlarm1(alarmtime, DS3231_A1_Second); // try in 15 seconds again
      delay(200);
      rtc.clearAlarm(1);
      delay(2000);
    }
  
  delay(100);
  wdt_reset();
  Wire.begin();
  delay(500);

  // RTC initialization
  tcaselect(addr_rtc);
  delay(100);
  while (! rtc.begin()) {
    delay(500);
    Serial.flush();
  }
  
  wdt_reset();
  
  if (rtc.lostPower()) {
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // write INTCN pin of RTC
  rtc.writeSqwPinMode(DS3231_OFF);
  // set alarm
  //rtc.clearAlarm(1);
  //rtc.clearAlarm(2);
  delay(100);
  DateTime alarmtime = rtc.now()+TimeSpan(0,0,0,1);
  rtc.setAlarm1(alarmtime, DS3231_A1_PerSecond);

  wdt_reset();
  // SD card initialization
  // Initialize SdFat or print a detailed error message and halt
  // Use half speed like the native library.
  // change to SPI_FULL_SPEED for more performance.
  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) sd.initErrorHalt();
  // open the file for write at end like the Native SD library
  if ( !sd.exists(filename) ) { 
    if (!dataFile.open(filename, O_RDWR | O_CREAT | O_AT_END)) {
      sd.errorHalt(F("opening file (setup) for write failed"));
    }
    // if the file opened okay, write to it:
    dataFile.println(F("time;T_sht;RH_sht;Comment"));
    // close the file:
    dataFile.close();
    wdt_reset();
    delay(800);
  }

  wdt_reset();

  // sensor initialization
  delay(100);
  tcaselect(addr_sht_1);
  delay(100);
  sht_1.init();
  
  wdt_reset();

  delay(100);
  tcaselect(addr_sht_2);
  delay(100);
  sht_2.init();
  
  wdt_reset();

  delay(100); 
}

void loop() {
  // Sensor 1: messe "meastime" sekunden lang
  float t = millis();
  float endt = t + 1000*meastime;

  // initialize needed values
  T_mn = 0;
  RH_mn = 0;
  count = 0;
  //wdt_disable();
  tcaselect(addr_sht_1);
  wdt_reset();
  // wdt_enable(WDTO_2S);
  delay(100);
  while ( (t <= endt) ) {
    t = millis();
    wdt_reset();
    if ( sht_1.readSample() ) {
      RH = sht_1.getHumidity();//reads humidity
      // Read temperature as Celsius (the default)
      T = sht_1.getTemperature();
      RH_mn = RH_mn+RH;
      T_mn = T_mn + T;
      count = count+1;
      delay(pausetime);
    }
  }
  if ( count > 0 ) {
    RH_mn = RH_mn / count;
    T_mn = T_mn / count;
    com = String(F("sht31_1 | #measurements = "))+String(count)+String(F(" | measturingtime = "))+String(meastime)+String(F(" sec"));
    writeData(T_mn, RH_mn, com);
  }

  
  // Sensor 2: messe "meastime" sekunden lang
  t = millis();
  endt = t + 1000*meastime;
  wdt_reset();

  // initialize needed values
  T_mn = 0;
  RH_mn = 0;
  count = 0;
  
  tcaselect(addr_sht_2);
  delay(100);
  while ( (t <= endt) ) {
    t = millis();
    wdt_reset();
    if ( sht_2.readSample() ) {
      RH = sht_2.getHumidity();//reads humidity
      // Read temperature as Celsius (the default)
      T = sht_2.getTemperature();
      RH_mn = RH_mn+RH;
      T_mn = T_mn + T;
      count = count+1;
      delay(pausetime);
    }
  }
  if ( count > 0 ) {
    RH_mn = RH_mn / count;
    T_mn = T_mn / count;
    com = String(F("sht31_2 | #measurements = "))+String(count)+String(F(" | measturingtime = "))+String(meastime)+String(F(" sec"));
    writeData(T_mn, RH_mn, com );
  }
  
  // Reset alarm for next wakeup and go to sleep!
  tcaselect(addr_rtc);
  delay(100);
  wdt_reset();
  rtc.clearAlarm(2);
  rtc.disableAlarm(2);
  free(p);
  DateTime alarmtime = rtc.now()+TimeSpan(0,0,timeinterval,0);
  rtc.setAlarm1(alarmtime, DS3231_A1_Minute);
  delay(200);
  rtc.clearAlarm(1);
  delay(5000);
}

  /**
  * the writeData function gets the humidity (h), temperature in celsius (t) and farenheit (f) as input
  * parameters. It uses the RTC to create a filename using the current date, and writes the temperature
  * and humidity with a date stamp to this file
  */
void writeData(float T,float RH, String comment) {
  wdt_reset();
  tcaselect(addr_rtc);
  delay(10);
  DateTime p = rtc.now();
  delay(10);
  // switch to full speed when soldered (better performance)
  if (!sd.begin(chipSelect, SPI_FULL_SPEED)) sd.initErrorHalt();
  // open the file for write at end like the Native SD library
  if (!dataFile.open(filename, O_WRITE | O_APPEND)) {
    sd.errorHalt(F("writeData(): opening file (writedata) for write failed"));
  }
  delay(10);
  String tstring = String(p.year())+"-"+String(p.month())+"-"+String(p.day())+" "+String(p.hour())+":"+String(p.minute())+":"+String(p.second());
  // if the file opened okay, write to it:
  dataFile.println(tstring+"; "+String(T)+"; "+String(RH)+"; "+comment);
  delay(10);
  // close the file:
  dataFile.close();
  wdt_reset();
  delay(500);
}
